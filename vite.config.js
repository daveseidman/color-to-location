/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from 'vite';
import basicSsl from '@vitejs/plugin-basic-ssl';

export default defineConfig({
  plugins: [
    basicSsl(),
  ],
  server: {
    port: 8080,
    host: true,
    https: true,
    proxy: {
      '/socket.io': {
        target: 'ws://localhost:8000',
        ws: true,
      },
    },
  },
});
